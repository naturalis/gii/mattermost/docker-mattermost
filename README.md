docker_mattermost 
=====================
docker-compose definition for deployment of naturalis mattermost server.


Docker-compose
--------------

This is a complete docker-compose setup for mattermost. Which
consists of:

 - app
 - traefik 2.x

Postgres is not dockerized, and should be installed/configured seperately
